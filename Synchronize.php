<?php
PHP_SAPI === 'cli' || die('CLI ONLY');

class Synchronize
{
    function __construct()
    {

    }

    private function loadConfiguration($file)
    {
        $fp = null;
        
        if(!file_exists($file) || ($fp=fopen($file,'r')) === false)
        {
            echo 'File '.$file.' not found, please make sure your config file is readable and in the right location'.PHP_EOL;
            exit();
        }
        $configurations = array();
        $config = array();
        $title = '';

        while(($line = fgets($fp)) !== false)
        {
            $line = trim($line);
            if($line == ''){continue;}
            if(substr($line,0,1) == '#'){continue;}

            if(substr($line,0,1) == '[' && substr($line,-1) == ']')
            {
                if($title!='')
                {
                    $configurations[$title] = $config;
                    $config = array();
                }
                $title = substr($line,1,-1);
            }else
            {
                $parts = explode('=',$line,2);
                if(sizeof($parts) != 2)
                {
                    echo 'Malformed config line! '.$line.PHP_EOL;continue;
                }

                $parts[0]=trim($parts[0]);$parts[1]=trim($parts[1]);

                if($parts[0]=='query')
                {
                    while($parts[1]!=str_replace('  ',' ',$parts[1]))
                    {
                        $parts[1] = str_replace('  ',' ',$parts[1]);
                    }
                    $parts[1] = explode(' ',$parts[1]);
                    if(sizeof($parts[1]) != 4)
                    {
                        echo 'Malformed query! '.$line.PHP_EOL;continue;
                    }

                    $config['query'][$parts[1][0].' '.$parts[1][1].' '.$parts[1][2]] = $parts[1][3];
                }else if($parts[0]=='folder')
                {
                    if(isset($config['folder']))
                    {
                        $config['folder'][] = $parts[1];
                    }else
                    {
                        $config['folder'] = array($parts[1]);
                    }
                }else if($parts[0]=='customQuery')
                {
                    if(isset($config['customQuery']))
                    {
                        $config['customQuery'][] = $parts[1];
                    }else
                    {
                        $config['customQuery'] = array($parts[1]);
                    }
                }else
                {
                    $config[$parts[0]] = $parts[1];
                }
            }
        }
        $configurations[$title] = $config;
        fclose($fp);

        return $configurations;
    }

    private function selectConfig($options)
    {
        echo 'Select a configuration:'.PHP_EOL;
        for($i=1;$i<sizeof($options)+1;$i++)
        {
            echo '    '.$i.'. '.$options[$i-1].PHP_EOL;
        }

        $result = -1;
        if(function_exists('readline'))
        {
            $result = intval(readline());
        }else
        {
            $fp = fopen("php://stdin","r");
            $result = intval(trim(fgets($fp,8)));
            fclose($fp);
        }

        if($result <= 0 || $result > sizeof($options))
        {
            echo 'Invalid selection!'.PHP_EOL;exit();
        }
        return $options[$result-1];
    }

    private function mergeConfig($selection, $config)
    {
        if($selection=='default')
        {
            return $config['default'];
        }

        $result = $config['default'];
        foreach($config[$selection] as $key => $value)
        {
            if($key == 'query')
            {
                foreach($value as $_key => $_value)
                {
                    $result['query'][$_key] = $_value;
                }
            }else if($key == 'folder')
            {
                foreach($value as $_value)
                {
                    $result[$key][] = $_value;
                }
            }else
            {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    private function isValidConfig($config)
    {
        $compulsory = array('port','remote_user','remote_host','remote_webroot','remote_backupdir','local_webroot','local_backupdir','max_dump_age','config_table_name');
        $allowed = array_merge($compulsory,array('query','folder','customQuery'));
        $given = array_keys($config);

        $missing = array_diff($compulsory,$given);
        if(sizeof($missing) > 0)
        {
            foreach($missing as $miss)
            {
                echo 'Missing config value: '.$miss.PHP_EOL;
            }
            return false;
        }

        $undefined = array_diff($given,$allowed);
        if(sizeof($undefined) > 0)
        {
            foreach($undefined as $undef)
            {
                echo 'Unspecified config option: '.$undef.PHP_EOL;
            }
            return false;
        }

        return true;
    }

    private function sync($config)
    {
        $this->ssh_command = 'ssh -a -p '.$config['port'].' '.$config['remote_user'].'@'.$config['remote_host'].' ';
        $this->rsync_command = 'rsync -vcrup --exclude \'*.original\' -e \'ssh -a -p '.$config['port'].'\' '.$config['remote_user'].'@'.$config['remote_host'].':';
echo 'Searching remote db dump'.PHP_EOL;  
        $remoteName = $this->locateMostRecentDBdump($config['remote_backupdir']);
        if($remoteName===false || !$this->isDbDumpRecent($config['remote_backupdir'].'/'.$remoteName,$config['max_dump_age']))
        {
echo 'Creating db dump'.PHP_EOL;
            $remoteName = $this->createDatabaseDump($config['remote_backupdir'],$config['remote_webroot']);
        }else
        {
echo 'Suitable db dump found'.PHP_EOL;
        }
        $dumpName = $config['local_backupdir'].'/'.$remoteName;
        if(!file_exists($dumpName))
        {
echo 'Downloading db dump'.PHP_EOL;
            $this->getRemoteFile($config['remote_backupdir'].'/'.$remoteName,$dumpName);
        }else
        {
echo 'Reusing db dump'.PHP_EOL;
        }
        if(!file_exists($dumpName))
        {
            echo 'Could not retrieve remote db dump. aborting.'.PHP_EOL;exit(1);
        }
echo 'Importing db dump'.PHP_EOL;
        $this->importDatabase($dumpName,$config['local_webroot']);
        if(isset($config['query']))
        {
            $this->runQueries($config['query'],$config['local_webroot'],$config['config_table_name']);
        }
        if(isset($config['customQuery']))
        {
            $this->runCustomQueries($config['customQuery'],$config['local_webroot']);
        }
echo PHP_EOL;
        if(isset($config['folder']))
        {
echo 'Synchronizing filesystem'.PHP_EOL;
            $this->syncFiles($config['folder'],$config['remote_webroot'],$config['local_webroot']);
echo PHP_EOL;
        }
echo 'Done. Don\'t forget to flush the cache and run sys:setup:inc'.PHP_EOL;
    }

    private function syncFiles($files,$remote,$local)
    {
        foreach($files as $folder)
        {
echo '  Synchronizing '.$folder.' folder'.PHP_EOL;
            shell_exec("$this->rsync_command$remote/$folder/ $local/$folder");
        }
    }

    private function runCustomQueries($queries,$webroot)
    {
        foreach($queries as $query)
        {
echo 'Running query '.$query.PHP_EOL;
            shell_exec("n98-magerun --root-dir=$webroot db:query \"$query\"");
        }
    }

    private function runQueries($queries,$webroot,$core_config_data)
    {
        ksort($queries);
        foreach($queries as $key => $value)
        {
            $key = explode(' ',$key);
            $scope = trim($key[0]);
            $scopeId = trim($key[1]);
            $path = trim($key[2]);

            $scopeIdVal = '';
            $scopeVal = '';

            if($scope == '*')
            {
                $scope = '';
            }else
            {
                $scopeVal = "\"$scope\",";
                $scope = "scope,";
            }

            if($scopeId == '*')
            {
                $scopeId = '';
            }else
            {
                $scopeIdVal = "$scopeId,";
                $scopeId = "scope_id,";
            }

            $query = "INSERT INTO $core_config_data ($scope $scopeId path,value) VALUES ($scopeVal $scopeIdVal \"$path\",\"$value\") ON DUPLICATE KEY UPDATE value=\"$value\";";
echo 'Running query '.$query.PHP_EOL;
            shell_exec("n98-magerun --root-dir=$webroot db:query '$query'");
        }
    }

    private function importDatabase($source,$webroot)
    {
        $importCommand = "n98-magerun --root-dir=$webroot db:import -c gzip --drop $source";
        shell_exec($importCommand);
    }

    private function getRemoteFile($from, $to)
    {
        shell_exec($this->rsync_command.$from.' '.$to);
    }

    private function createDatabaseDump($dir,$readPath)
    {
        $dumpName = 'remote-'.date('Ymd-His').'.sql.gz';
        $dumpCommand = "n98-magerun --root-dir=$readPath db:dump --no-interaction --no-ansi --skip-root-check -c gzip $dir/$dumpName";
        shell_exec("$this->ssh_command\"$dumpCommand\"");
        return $dumpName;
    }

    private function isDBdumpRecent($path,$maxAge)
    {
        $timestamp = (trim(shell_exec($this->ssh_command.'stat -c%Z '.$path)));
        return intval($timestamp) + $maxAge > time();
    }

    private function locateMostRecentDBdump($dir)
    {
        $result = shell_exec($this->ssh_command.'ls -l --sort=time '.$dir);
        $result = explode(PHP_EOL,trim($result));
        if(sizeof($result) <= 1)
        {
            return false;
        }

        $result = explode(' ',$result[1]);
        $name = $result[sizeof($result)-1];

        return $name;
    }

    private function versionCheck($version,$config)
    {
        if($version != 'HEAD')
        {
            $fileName = "Synchronize.$version.php";
            if(!file_exists($fileName))
            {
                shell_exec("wget https://bitbucket.org/supportdeskbv/synchronize/raw/$version/Synchronize.php -O $fileName");
            }
            echo "Please run php $fileName -c $config".PHP_EOL;
            exit(0);
        }else
        {
            shell_exec("wget https://bitbucket.org/supportdeskbv/synchronize/raw/HEAD/Synchronize.php -O Synchronize.tmp");
            if(file_get_contents('Synchronize.php') != file_get_contents('Synchronize.tmp'))
            {
                rename('Synchronize.tmp','Synchronize.php');
                echo 'Synchronization script updated. Please restart the program'.PHP_EOL;
                exit(0);
            }else
            {
                unlink('Synchronize.tmp');
            }
        }
    }

    public function run($argv)
    {
        $configFile = 'Synchronize.config';
        $version = 'HEAD';
        $skipAutoUpdate = false;

        for($i=1;$i<sizeof($argv);$i++)
        {
            switch($argv[$i])
            {
                case '-c':
                    $i++;
                    $configFile = $argv[$i];
                    break;
                case '-u':
                    $i++;
                    $version = $argv[$i];
                    break;
                case '--skipAutoupdate':
                    $skipAutoUpdate = true;
                    break;
                case '-h':
                case '--help':
                    echo 'Usage: php '.$argv[0].' [-h|--help] | [-u version] [--skipAutoupdate] [-c configFile]'.PHP_EOL;
                    exit();
                default:
                    echo 'Unknown option: '.$argv[$i].'. Aborting'.PHP_EOL;
                    exit();
            }
        }

        if(!($version=='HEAD' && $skipAutoUpdate))
        {
            $this->versionCheck($version,$configFile);
        }

        $config = $this->loadConfiguration($configFile);
        $selection = $this->selectConfig(array_keys($config));
        $config = $this->mergeConfig($selection,$config);
        if($this->isValidConfig($config))
        {
            $this->sync($config);
        }
    }
}

(new Synchronize())->run($argv);
echo PHP_EOL;
